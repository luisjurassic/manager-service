﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.IO;
using System.Runtime.CompilerServices;

namespace Manager.Service
{
    public class Settings
    {
        private static readonly string LocalFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "settings.json");
        public static Settings Default { get; } = new Settings();

        private static bool Created { get; set; }

        public bool RunHideWindow
        {
            get
            {
                if (Created)
                    return false;
                else
                    return Find<bool>();
            }
            set
            {
                Save(value);
            }
        }

        public bool RunWindow
        {
            get
            {
                if (Created)
                    return false;
                else
                    return Find<bool>();
            }
            set
            {
                Save(value);
            }
        }

        public string Language
        {
            get
            {
                if (Created)
                    return "english";
                return Find<string>();
            }
            set
            {
                Save(value);
            }
        }

        private static void CreateDefaultSettings()
        {
            JObject jObject = JObject.Parse(JsonConvert.SerializeObject(Default));
            File.WriteAllText(LocalFile, jObject.ToString());
            Created = false;
        }

        private static T Find<T>([CallerMemberName]string setting = null)
        {
            if (!File.Exists(LocalFile))
            {
                Created = true;
                CreateDefaultSettings();
            }

            string content = File.ReadAllText(LocalFile);
            var jo = JObject.Parse(content);
            var temp = jo[setting];
            if (temp.Type == JTokenType.Array)
                return JsonConvert.DeserializeObject<T>(temp.ToString());
            else
                return jo.Value<T>(setting);
        }

        private static void Save(object value, [CallerMemberName]string setting = null)
        {
            string content = File.ReadAllText(LocalFile);
            JObject jObject = JObject.Parse(content);

            if (value.GetType().IsGenericType && value is IEnumerable)
                jObject[setting] = JToken.FromObject(value);
            else
                jObject[setting] = new JValue(value);

            File.WriteAllText(LocalFile, jObject.ToString());
        }
    }
}
