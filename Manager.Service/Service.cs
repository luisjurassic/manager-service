﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Manager.Service
{
    public class Service : INotifyPropertyChanged
    {
        private CancellationTokenSource _Cancellation;
        private bool _CanRestart;
        private bool _CanStart;
        private bool _CanStop;
        private int _DelayTime;
        private string _ServiceName;
        private Settings _Settings;
        private ServiceControllerStatus _Status;
        private Brush _StatusColor;
        private string _StatusDescription;
        private ServiceType _ServiceType;

        public Service(string nameService)
        {
            DelayTime = 500;
            Settings = Settings.Default;
            NameService = nameService;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public bool CanRestart
        {
            get
            {
                return _CanRestart;
            }
            set
            {
                _CanRestart = value;
                OnPropertyChanged();
            }
        }

        public bool CanStart
        {
            get
            {
                return _CanStart;
            }
            set
            {
                _CanStart = value;
                OnPropertyChanged();
            }
        }

        public bool CanStop
        {
            get
            {
                return _CanStop;
            }
            set
            {
                _CanStop = value;
                OnPropertyChanged();
            }
        }

        public int DelayTime
        {
            get
            {
                return _DelayTime;
            }
            set
            {
                _DelayTime = value;
                OnPropertyChanged();
            }
        }

        public string ServiceName
        {
            get
            {
                return _ServiceName;
            }
            private set
            {
                _ServiceName = value;
                OnPropertyChanged();
            }
        }

        public ServiceType ServiceType
        {
            get
            {
                return _ServiceType;
            }
            private set
            {
                _ServiceType = value;
                OnPropertyChanged();
            }
        }

        public Settings Settings
        {
            get
            {
                return _Settings;
            }
            set
            {
                _Settings = value;
                OnPropertyChanged();
            }
        }

        public ServiceControllerStatus Status
        {
            get
            {
                return _Status;
            }
            set
            {
                _Status = value;
                OnPropertyChanged();
            }
        }

        public Brush StatusColor
        {
            get
            {
                return _StatusColor;
            }
            set
            {
                _StatusColor = value;
                OnPropertyChanged();
            }
        }

        public string StatusDescription
        {
            get
            {
                return _StatusDescription;
            }
            set
            {
                _StatusDescription = value;
                OnPropertyChanged();
            }
        }

        private string NameService { get; }

        private ServiceController ServiceControl
        {
            get
            {
                return new ServiceController(NameService);
            }
        }

        public void Finish()
        {
            if (!_Cancellation.IsCancellationRequested)
                _Cancellation.Cancel();
        }

        public void Initialize()
        {
            ServiceName = ServiceControl.DisplayName;
            ServiceType = ServiceControl.ServiceType;

            _Cancellation = new CancellationTokenSource();
            CancellationToken token = _Cancellation.Token;
            Task.Factory.StartNew(() =>
            {
                while (!token.IsCancellationRequested)
                {
                    try
                    {
                        if (token.IsCancellationRequested)
                            token.ThrowIfCancellationRequested();

                        ServiceStatus();
                        Task.Delay(DelayTime).Wait();
                    }
                    catch (OperationCanceledException)
                    {
                        return;
                    }
                    catch (Exception ex)
                    {
                        Utils.MessageError(ex, "Error fetching status.");
                    }
                }
            }, token);
        }

        public void OnPropertyChanged([CallerMemberName] string name = null) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

        public async void Restart()
        {
            try
            {
                if (ServiceControl.Status != ServiceControllerStatus.Stopped)
                {
                    await Task.Run(() => ServiceControl.Stop());

                    while (ServiceControl.Status != ServiceControllerStatus.Stopped)
                        Task.Delay(1000).Wait();

                    await Task.Run(() => ServiceControl.Start());
                }
            }
            catch (Exception ex)
            {
                Utils.MessageError(ex, "Service restart error");
            }
        }

        public async void Start()
        {
            try
            {
                if (Status != ServiceControllerStatus.Running)
                    await Task.Run(() => ServiceControl.Start());
            }
            catch (Exception ex)
            {
                Utils.MessageError(ex, "Service start error");
            }
        }

        public async void Stop()
        {
            try
            {
                if (ServiceControl.Status != ServiceControllerStatus.Stopped)
                    await Task.Run(() => ServiceControl.Stop());
            }
            catch (Exception ex)
            {
                Utils.MessageError(ex, "Service stop error");
            }
        }

        private void ServiceStatus()
        {
            Status = ServiceControl.Status;
            switch (Status)
            {
                case ServiceControllerStatus.ContinuePending:
                    StatusDescription = "Continue pending...";
                    StatusColor = Brushes.Blue;
                    CanStart = false;
                    CanStop = false;
                    CanRestart = false;
                    break;
                case ServiceControllerStatus.Paused:
                    StatusDescription = "Paused";
                    StatusColor = Brushes.Orange;
                    CanStart = true;
                    CanStop = true;
                    CanRestart = false;
                    break;
                case ServiceControllerStatus.PausePending:
                    StatusDescription = "Pause pending...";
                    StatusColor = Brushes.Blue;
                    CanStart = false;
                    CanStop = false;
                    CanRestart = false;
                    break;
                case ServiceControllerStatus.Running:
                    StatusDescription = "Running";
                    StatusColor = Brushes.Green;
                    CanStart = false;
                    CanStop = true;
                    CanRestart = true;
                    break;
                case ServiceControllerStatus.StartPending:
                    StatusDescription = "Start pending...";
                    StatusColor = Brushes.Blue;
                    CanStart = false;
                    CanStop = false;
                    CanRestart = false;
                    break;
                case ServiceControllerStatus.Stopped:
                    StatusDescription = "Stopped";
                    StatusColor = Brushes.Red;
                    CanStart = true;
                    CanStop = false;
                    CanRestart = false;
                    break;
                case ServiceControllerStatus.StopPending:
                    StatusDescription = "Stop pending...";
                    StatusColor = Brushes.Blue;
                    CanStart = false;
                    CanStop = false;
                    CanRestart = false;
                    break;
            }
        }
    }
}
