﻿using System;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;

namespace Manager.Service
{
    class IconChanged
    {
        public NotifyIcon Notify { get; set; }
        private readonly MainWindow window;
        public string ToolTip { get; private set; }

        public IconChanged(MainWindow window, string toolTip)
        {
            this.window = window;
            Notify = new NotifyIcon();
            ToolTip = toolTip;
            NotifyIconCreated();
        }

        private void NotifyIconCreated()
        {
            Notify.Text = ToolTip;
            Notify.Icon = new Icon(Utils.GetStreamResourcePath("manager.service.ico"));
            Notify.Visible = true;

            CreateMenuStrip();
            Notify.MouseDoubleClick += Notifyicon_MouseDoubleClick;
        }

        private void CreateMenuStrip()
        {
            ContextMenuStrip cMenu = new ContextMenuStrip();
            cMenu.Items.Add(new ToolStripMenuItem(StyleController.GetLanguageValue("show"), null, new EventHandler(Show_Click), "show"));
            cMenu.Items.Add(new ToolStripMenuItem(StyleController.GetLanguageValue("hide"), null, new EventHandler(Hide_Click), "hide"));
            cMenu.Items.Add(new ToolStripSeparator());
            cMenu.Items.Add(new ToolStripMenuItem(StyleController.GetLanguageValue("restart"), Image.FromStream(Utils.GetStreamResourcePath("restart.png")), new EventHandler(Restart_Click), "restart"));
            cMenu.Items.Add(new ToolStripMenuItem(StyleController.GetLanguageValue("start"), Image.FromStream(Utils.GetStreamResourcePath("start.png")), new EventHandler(Start_Click), "start"));
            cMenu.Items.Add(new ToolStripMenuItem(StyleController.GetLanguageValue("stop"), Image.FromStream(Utils.GetStreamResourcePath("stop.png")), new EventHandler(Stop_Click), "stop"));

            cMenu.Items.Add(new ToolStripSeparator());

            cMenu.Items.Add(new ToolStripMenuItem(StyleController.GetLanguageValue("exit"), Image.FromStream(Utils.GetStreamResourcePath("exit.png")), new EventHandler(Exit_Click), "exit"));
            Notify.ContextMenuStrip = cMenu;
        }

        private void Show_Click(object sender, EventArgs e)
        { 
            ShowVisual();
        }

        private void Hide_Click(object sender, EventArgs e)
        {
            Notify.ContextMenuStrip.Items["show"].Enabled = true;
            Notify.ContextMenuStrip.Items["hide"].Enabled = false;
            window.Close();
        }

        private void Stop_Click(object sender, EventArgs e)
        {
            window.StopClick(sender, null);
        }

        private void Start_Click(object sender, EventArgs e)
        {
            window.StartClick(sender, null);
        }

        private void Restart_Click(object sender, EventArgs e)
        {
            window.RestartClick(sender, null);
        }

        private void Notifyicon_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            ShowVisual();
        }

        private void ShowVisual()
        {
            Notify.ContextMenuStrip.Items["show"].Enabled = false;
            Notify.ContextMenuStrip.Items["hide"].Enabled = true;
            window.Service.DelayTime = 500;
            window.Show();
        }

        public void Exit_Click(object sender, EventArgs e)
        {
            System.Windows.Application.Current.Shutdown();
        }

        public void Dispose()
        {
            Notify.Visible = false;
            Notify.Dispose();
        }

        ~IconChanged()
        {
            Notify = null;
        }
    }
}
