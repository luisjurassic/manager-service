﻿using Microsoft.Win32;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Manager.Service
{
    public partial class MainWindow : Window
    {
        private const string SERVICE_NAME =
#if DEBUG
            "WSearch";
#else
            "Service1";
#endif
        private readonly Assembly executable = Assembly.GetExecutingAssembly();
        public Service Service { get; set; }
        private IconChanged IconChanged { get; }

        public MainWindow()
        {
            InitializeComponent();
            Closed += (s, e) => { Service.Finish(); };
            MouseDown += (s, e) => { MouseDragMove(e); };
            Loaded += (s, e) => { WindownLoaded(); };
            Closing += WindowClosing;
            DataContext = Service = new Service(SERVICE_NAME);
            Service.Initialize();
            Icon = BitmapFrame.Create(Utils.GetStreamResourcePath("manager.service.ico"));
            IconChanged = new IconChanged(this, Service.ServiceName);
            if (Settings.Default.RunHideWindow)
                Close();
        }

        private void CheckBoxChecked(object sender, RoutedEventArgs e)
        {
            try
            {
                RegistryKey registryLocalMachine = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);
                if (registryLocalMachine.GetValue(SERVICE_NAME) == null)
                    registryLocalMachine.SetValue(executable.GetName().Name, executable.Location);
            }
            catch (Exception ex)
            {
                Utils.MessageError($"Erro ao criar registro no Windows. {ex.Message}\n\n HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run\\{SERVICE_NAME}", "Erro ao Registrar Serviço");
            }
        }

        private void CheckBoxUnchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                RegistryKey registryLocalMachine = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);
                if (registryLocalMachine.GetValue(SERVICE_NAME) != null)
                    registryLocalMachine.DeleteValue(executable.GetName().Name, false);
            }
            catch (Exception ex)
            {
                Utils.MessageError($"Erro ao criar registro no Windows.{ex.Message}\n\n HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run\\{SERVICE_NAME}", "Erro ao Registrar Serviço");
            }
        }

        private void CloseClick(object sender, RoutedEventArgs e)
        {
            Close();
        }

        public void RestartClick(object sender, RoutedEventArgs e)
        {
            Service.Restart();
        }

        public void StartClick(object sender, RoutedEventArgs e)
        {
            Service.Start();
        }

        public void StopClick(object sender, RoutedEventArgs e)
        {
            Service.Stop();
        }

        private void WindowClosing(object sender, CancelEventArgs e)
        {
            e.Cancel = true;
            Service.DelayTime = 30000;
            IconChanged.Notify.ContextMenuStrip.Items["show"].Enabled = true;
            IconChanged.Notify.ContextMenuStrip.Items["hide"].Enabled = false;
            Hide();
        }

        private void MouseDragMove(MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                DragMove();
        }

        private void WindownLoaded()
        {
            IconChanged.Notify.ContextMenuStrip.Items["show"].Enabled = WindowState == WindowState.Minimized;
            IconChanged.Notify.ContextMenuStrip.Items["hide"].Enabled = WindowState != WindowState.Minimized;
        }

        private void ButtonIsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            IconChanged.Notify.ContextMenuStrip.Items["restart"].Enabled = Service.CanRestart;
            IconChanged.Notify.ContextMenuStrip.Items["start"].Enabled = Service.CanStart;
            IconChanged.Notify.ContextMenuStrip.Items["stop"].Enabled = Service.CanStop;
        }
    }
}
