﻿using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Windows;
using System.Windows.Data;

namespace Manager.Service
{
    public static class Utils
    {
        public static void MessageError(Exception ex, string tittle)
        {
            var msg = string.Empty;
            do
            {
                msg += ex.Message + "\n";
                ex = ex.InnerException;
            }
            while (ex != null);

            MessageBox.Show(msg, tittle, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public static void MessageError(string msg, string tittle)
        {
            MessageBox.Show(msg, tittle, MessageBoxButton.OK, MessageBoxImage.Error);
        }

        public static void MessageInfo(string msg, string tittle)
        {
            MessageBox.Show(msg, tittle, MessageBoxButton.OK, MessageBoxImage.Information);
        }

        public static void MessageAlert(string msg, string tittle)
        {
            MessageBox.Show(msg, tittle, MessageBoxButton.OK, MessageBoxImage.Exclamation);
        }

        public static bool MessageDialog(string msg, string tittle, MessageBoxButton messageButtons)
        {
            return (MessageBox.Show(msg, tittle, messageButtons, MessageBoxImage.Question) == MessageBoxResult.Yes);
        }

        public static Stream GetStreamResourcePath(string manifestFileLocation)
        {
            var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Manager.Service.Resources." + manifestFileLocation);
            return stream;
        }

        private static void LocalGrantAccess(string fullPath)
        {
            DirectoryInfo dInfo = new DirectoryInfo(fullPath);
            DirectorySecurity dSecurity = dInfo.GetAccessControl();
            var security = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
            var fullControl = FileSystemRights.FullControl;
            var flags = InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit;
            var propagation = PropagationFlags.NoPropagateInherit;
            var access = AccessControlType.Allow;
            dSecurity.AddAccessRule(new FileSystemAccessRule(security, fullControl, flags, propagation, access));
            dInfo.SetAccessControl(dSecurity);
        }
    }

    public class StyleController
    {
        private const string DEFAULT_LANGUAGE_LOCAL = "/Manager.Service;component/Language/English.xaml";

        public static string GetLanguageValue(string keyName)
        {

            ResourceDictionary lang = Application.LoadComponent(new Uri(DEFAULT_LANGUAGE_LOCAL,UriKind.Relative)) as ResourceDictionary;
            return lang[keyName].ToString();
        }
    }
}

namespace Manager.Service.Converters
{
    [ValueConversion(typeof(bool), typeof(bool))]
    public class BooleanValueConvert : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter == null)
                return value;

            return value.Equals(bool.Parse(parameter.ToString()));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
